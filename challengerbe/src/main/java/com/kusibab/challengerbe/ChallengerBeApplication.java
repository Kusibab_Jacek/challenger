package com.kusibab.challengerbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class challengerBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(challengerBeApplication.class, args);
	}

}
